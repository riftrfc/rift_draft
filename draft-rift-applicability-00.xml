<?xml version="1.0" encoding="iso-8859-1" ?>
<!--<!DOCTYPE rfc SYSTEM "rfc4748.dtd"> -->
<!DOCTYPE rfc SYSTEM 'rfc2629.dtd' [
        <!ENTITY RFC2328 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.2328.xml">
        <!ENTITY RFC5357 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.5357.xml">
        <!ENTITY RFC7130 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.7130.xml">
        <!ENTITY RFC4861 SYSTEM "http://xml.resource.org/public/rfc/bibxml/reference.RFC.4861.xml">
        <!ENTITY I-D.ietf-rift-rift PUBLIC '' 'http://xml.resource.org/public/rfc/bibxml3/reference.I-D.ietf-rift-rift.xml'>
        <!ENTITY I-D.white-distoptflood PUBLIC '' 'http://xml.resource.org/public/rfc/bibxml3/reference.I-D.white-distoptflood.xml'>
        ]>

<?xml-stylesheet type='text/xsl' href='rfc2629.xslt' ?>

<?rfc toc="yes"?>
<?rfc symrefs="yes"?>
<?rfc sortrefs="yes"?>
<?rfc disable-output-escaping="yes"?>

<rfc category="std"  docName="draft-zhang-rift-yang-02"
     ipr="trust200902">
    <!-- ***** FRONT MATTER ***** -->
    <front>
        <title abbrev="RIFT Applicability Statement">RIFT Applicability</title>

        <author fullname="Dmitry Afanasiev" initials="Dmitry" surname="Afanasiev">
            <organization>Yandex</organization>
            <address>
                <postal>
                    <street></street>

                    <city></city>

                    <region></region>

                    <code></code>

                    <country></country>
                </postal>

                <email>fl0w@yandex-team.ru</email>
            </address>
        </author>

        <author fullname="" initials="Zheng" surname="Zhang">
            <organization>ZTE Corporation</organization>
            <address>
                <postal>
                    <street></street>

                    <city></city>

                    <region></region>

                    <code></code>

                    <country></country>
                </postal>

                <email>zzhang_ietf@hotmail.com</email>
            </address>
        </author>

        <author fullname="Yuehua Wei" initials="Yuehua" surname="Wei">
            <organization>ZTE Corporation</organization>
            <address>
                <postal>
                    <street></street>

                    <city></city>

                    <region></region>

                    <code></code>

                    <country></country>
                </postal>

                <email>wei.yuehua@zte.com.cn</email>
            </address>
        </author>

        <author fullname="Tom Verhaeg" initials="Tom" surname="Verhaeg">
            <organization>Interconnect Services B.V.</organization>
            <address>
                <postal>
                    <street></street>

                    <city></city>

                    <region></region>

                    <code></code>

                    <country></country>
                </postal>

                <email>t.verhaeg@interconnect.nl</email>
            </address>
        </author>

        <author fullname="Jaroslaw Kowalczyk" initials="Jaroslaw" surname="Kowalczyk">
            <organization>Orange Polska</organization>
            <address>
                <postal>
                    <street></street>

                    <city></city>

                    <region></region>

                    <code></code>

                    <country></country>
                </postal>

                <email>jaroslaw.kowalczyk2@orange.com</email>
            </address>
        </author>

        <date year="2019"/>
        <area>Routing</area>
        <workgroup>RIFT WG</workgroup>
        <keyword>RIFT</keyword>
        <abstract>
            <t>
                This document discusses the properties and applicability of RIFT in different
                network topologies.  It intends to provide a
                rough guide how RIFT can be deployed to simplify routing operations in
                Clos topologies and their variations.
            </t>
        </abstract>
    </front>

    <!-- ***** MIDDLE MATTER ***** -->

    <middle>
        <section title="Introduction">

            <t>This document intends to explain the properties and applicability of
                <xref target="I-D.ietf-rift-rift">RIFT</xref> in different
                deployment scenarios and highlight the operational simplicity of the technology compared
                to traditional routing solutions.
            </t>

        </section>

        <section title="Preconditions">

            <t>
                Further content of this document assumes that the reader is
                familiar with the
                terms and concepts used in <xref target="RFC2328">OSPF</xref>
                and <xref target="ISO10589-Second-Edition">IS-IS</xref> link-state protocols and
                at least the sections of  <xref target="I-D.ietf-rift-rift">RIFT</xref> outlining
                the requirement of routing in IP fabrics and RIFT protocol concepts.
            </t>

            <section title="Applicable Topologies">

                <t>
                    Albeit RIFT is specified primarily for "proper" Clos or "fat-tree" structures,
                    it already supports PoD concepts which are strictly speaking not found in
                    original Clos concepts.
                </t>
                <t>Further, the specification explains and supports operations of multi-plane
                    Clos variants where the protocol relies on set of rings to allow the
                    reconciliation of topology view of different planes as most desirable solution
                    making proper disaggregation viable in case of failures.
                    This observations hold not only in case of RIFT but in the generic
                    case of dynamic routing on Clos variants with multiple planes and failures
                    in bi-sectional bandwidth, especially on the leafs.
                </t>

                <section title="Horizontal Links">
                    <t>
                        RIFT is not limited to pure Clos divided into PoD and multi-planes but
                        supports horizontal links below the top of fabric level. Those links
                        are used however only as routes of last resort when a spine looses all
                        northbound links or cannot compute a default route through them.
                    </t>
                </section>

                <section title="Vertical Shortcuts">
                    <t>
                        Through relaxations of the specified adjacency forming rules
                        RIFT implementations can be extended to support vertical "shortcuts" as
                        proposed by e.g. <xref target="I-D.white-distoptflood"/>. The RIFT specification
                        itself does not provide the exact details since the resulting solution suffers from
                        either much larger blast radii with increased flooding volumes or
                        in case of maximum aggregation routing bow-tie problems.
                    </t>
                </section>

            </section>

            <section title="Use Cases">

                <section title="DC Fabrics">
                    <t>
                        RIFT is largely driven by demands and hence ideally suited for application
                        in underlay of
                        data center IP fabrics, vast majority of which seem to be currently (and
                        for
                        the foreseeable future)
                        Clos architectures. It significantly simplifies operation and deployment
                        of such fabrics as described in <xref target="opex"/> for environments compared
                        to
                        extensive proprietary provisioning and operational solutions.
                    </t>
                </section>

                <section title="Metro Fabrics">
                    <t>
                        The demand for bandwidth is increasing steadily, driven primarily by
                        environments close to
                        content producers (server farms connection via DC fabrics) but in
                        proximity to content consumers as well.
                        Consumers are often clustered in metro areas with their own network
                        architectures that can benefit
                        from simplified, regular Clos structures and hence
                        RIFT.

                    </t>
                </section>

                <section title="Building Cabling">
                <t>
                    Commercial edifices are often cabled in topologies that are
                    either Clos or its isomorphic equivalents. With many floors the
                    Clos can grow rather high and with that present a challenge
                    for traditional routing protocols (except BGP and by now largely
                    phased-out PNNI) which do not support
                    an arbitrary number of levels which RIFT does naturally. Moreover,
                    due to limited sizes of forwarding tables in active elements
                    of building cabling the minimum FIB size RIFT maintains under
                    normal conditions can prove particularly cost-effective in terms of
                    hardware and operational costs.
                </t>
                </section>
                
                <section title="Internal Router Switching Fabrics">
                    <t>
                        It is common in high-speed communications switching and routing
                        devices to use fabrics when a crossbar is not feasible due to cost,
                        head-of-line blocking 
                        or size trade-offs. Normally such fabrics are not self-healing or rely
                        on 1:/+1 protection schemes but it is conceivable to use RIFT to
                        operate Clos fabrics that can deal effectively with interconnections
                        or subsystem failures in such module. RIFT is neither IP specific and
                        hence any link addressing connecting internal device subnets is
                        conceivable.
                    </t>
                </section>

            </section>

        </section>

        <section title="Operational Simplifications and Considerations" anchor="opex">

            <t>
                RIFT presents the opportunity for organizations building and operating
                IP fabrics to simplify their operation and deployments while achieving
                many desirable
                properties of a dynamic routing on such a substrate:
                <list style="symbols">
                    <t>
                        RIFT design follows minimum blast radius and minimum necessary
                        epistomological scope philosophy which leads to very good scaling
                        properties while delivering maximum reactiveness.
                    </t>
                    <t>
                        RIFT allows for extensive Zero Touch Provisioning within the protocol.
                        In its most extreme version RIFT does not rely on any specific addressing
                        and for IP fabric can operate using <xref target="RFC4861">IPv6 ND</xref> only.
                    </t>
                    <t>
                        RIFT has provisions to detect common IP fabric mis-cabling scenarios.
                    </t>
                    <t>
                        RIFT negotiates automatically BFD per link allowing this way for IP and <xref
                            target="RFC7130">micro-BFD</xref> to replace LAGs which do hide bandwidth
                        imbalances in case of constituent failures. Further automatic link validation
                        techniques similar to <xref target="RFC5357"/> could be supported as well.
                    </t>
                    <t>
                        RIFT inherently solves many difficult problems associated with the use of
                        traditional routing topologies with dense meshes and high degrees of ECMP by
                        including automatic bandwidth balancing, flood reduction and automatic
                        disaggregation on failures while providing maximum aggregation of prefixes
                        in default scenarios.
                    </t>
                    <t>
                        RIFT reduces FIB size towards the bottom of the IP fabric where most nodes
                        reside and allows with that for cheaper hardware on the edges and introduction
                        of modern IP fabric architectures that encompass e.g. server multi-homing.
                    </t>
                    <t> RIFT provides valley-free
                        routing and with that is loop free. This allows the use of any such valley-free
                        path
                        in bi-sectional fabric bandwidth between two destination irregardless of their
                        metrics which can be used to balance load on the fabric in different ways.
                    </t>
                    <t>
                        RIFT includes a key-value distribution mechanism
                        which allows for many future applications
                        such as automatic provisioning of basic overlay services or automatic key
                        roll-overs over whole fabrics.
                    </t>
                    <t>
                        RIFT is designed for minimum delay in case of prefix mobility on the fabric.
                    </t>
                    <t>
                        Many further operational and design points collected over many years of
                        routing protocol deployments have been incorporated in RIFT such as
                        fast flooding rates, protection of information lifetimes and operationally
                        easily recognizable remote ends of links and node names.
                    </t>

                </list>
            </t>

        </section>

    </middle>

    <back>

        <references title='Normative References'>
            <reference anchor="ISO10589-Second-Edition">

                <front>
                    <title>Intermediate system to Intermediate system intra-domain
                        routeing information exchange protocol for use in
                        conjunction with the protocol for providing the
                        connectionless-mode Network Service (ISO 8473)</title>

                    <author>
                        <organization>International Organization for Standardization</organization>
                    </author>
                    <date  month="Nov" year="2002"/>
                </front>
            </reference>

            &RFC2328;
            &RFC4861;
            &RFC5357;
            &RFC7130;
            &I-D.ietf-rift-rift;
            &I-D.white-distoptflood;
        </references>

    </back>
</rfc>