<?xml version="1.1" encoding="US-ASCII"?>
<!DOCTYPE rfc SYSTEM "rfc2629.dtd" [
<!ENTITY RFC2119 SYSTEM "http://xml2rfc.ietf.org/public/rfc/bibxml/reference.RFC.2119.xml">
<!ENTITY RFC8402 SYSTEM "http://xml2rfc.ietf.org/public/rfc/bibxml/reference.RFC.8402.xml">
]>
<?rfc toc="yes"?>
<?rfc tocompact="yes"?>
<?rfc tocdepth="3"?>
<?rfc tocindent="yes"?>
<?rfc symrefs="yes"?>
<?rfc sortrefs="yes"?>
<?rfc strict="no"?>
<?rfc rfcedstyle="yes"?>
<?rfc comments="yes"?>
<?rfc inline="yes"?>
<?rfc compact="yes"?>
<?rfc subcompact="no"?>
<rfc category="std" docName="draft-zzhang-rift-sr-00" ipr="trust200902">
  <front>
    <title abbrev="srift">SRIFT: Segment Routing In Fat Trees</title>

    <author fullname="Zhaohui Zhang" initials="Z." surname="Zhang">
      <organization>Juniper Networks</organization>
      <address>
        <email>zzhang@juniper.net</email>
      </address>
    </author>

    <date year="2018"/>

    <workgroup>RIFT</workgroup>

    <abstract>
    <t>
This document specifies signaling procedures for Segment Routing [RFC8402] with RIFT.
Each node's loopback address, Segment Routing Global Block (SRGB) and Node
Segment Identifier (SID), which must be unique within the SR domain and
are typically assigned by SR controllers or management, are
distributed southbound from the Top Of Fabric (TOF) nodes via the Key-Value
distribution mechanism, so that each node can compute how to reach a node
represented by the topmost Node SIDs .  For an ingress node to send SR traffic
to another node via an explicit path, an SR controller signals the
corresponding label stack to the ingress node so that the ingress node can
send packets accordingly.
    </t>
    </abstract>

    <note title="Requirements Language">
      <t>The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT",
      "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this
      document are to be interpreted as described in RFC2119.
      </t>
    </note>
  </front>

  <middle>
    <section title="Introduction">
    <t>
Before we discuss the SR procedures for RIFT, let us first review how SR works
with OSPF/ISIS <xref target="I-D.ietf-ospf-segment-routing-extensions"/>
<xref target="I-D.ietf-isis-segment-routing-extensions"/>.
    </t>
    <t>
Each node is provisioned with a loopback address, an SRGB, and a Node SID.
The loopback address and Node SID are co-ordinated centrally - it is unique
for each node across the SR network - and then communicated out of band to
each node and stored as configuration information. For example, the out of
band communication could be via primitive pen and paper, or modern signaling
from controllers.
    </t>
    <t>
SRGB configuration can be local to each node and different node can have the
same or different label blocks for flexible label allocation. Typically,
modern SR networks have identical SRGB on each node so that a Node SID
corresponds to the same label on each node. However that is not mandatory.
Either way, the SRGB is part of the node's local configuration. In today's
network it is very likely pushed down from some controllers.
    </t>
    <t>
Each node will then signal its SRGB, and its Node SID. The Node SID is just
an index into the SRGB and other nodes will derive the corresponding label
for each advertised Node SID. Consider the following example:
    <figure>
	<artwork>

                        B
                      *   *
                    *       *
                  *           *
                A               D
                  *           *
                    *       *
                      *   *
                        C                

   Node Name  Loopback   Node SID SRGB Label Base  SRGB Label Range
   ---------  --------   -------- ---------------  ----------------
   
   A          10.1.1.1      1     100              50
   B          10.1.1.2      2     100              50
   C          10.1.1.3      3     200              50
   D          10.1.1.4      4     100              50
	</artwork>
    </figure>
    </t>
    <t>
   Node A computes its IP and label routes as following:
    <figure>
	<artwork>

   Destination    Next Hop
   -----------    --------
   10.1.1.1       local
   10.1.1.2       if_ab
   10.1.1.3       if_ac
   10.1.1.4       if_ab, if_ac
   
   Label           Next Hop
   -----           --------

   100 (La_a)      pop and look up next header
   101 (Lb_a)      swap to 101, send out of if_ab
   102 (Lc_a)      swap to 202, send out of if_ac
   103 (Ld_a)      swap to 103, send out of if_ab
                   swap to 203, send out of if_ac

	</artwork>
    </figure>
    </t>
    <t>
For example, Node A computes the route to node D (represented by its loopback
address) and the next hops are node B via interface if_ab and node C via if_ac.
A uses D's Node SID (advertised along with D's loopback address), and index it
into its own SRGB to obtain label Ld_a (103). It also uses D's Node SID to
index into B's and C's SRGBs to obtain label Ld_b (103) and Ld_c (203)
respectively. Now it programs its label forwarding state with
(Ld_a --> (outgoing if_ab swap to Ld_b, outgoing if_ac swap to Ld_c)).
Notice that Ld_a, Ld_b and Ld_c are most likely the same though not
necessarily so.
    </t>
    <t>
Node B computes the route to D and finds that the next hop is node D itself
via interface if_bd.  It use D's Node SID (advertised along with D's loopback
address) and index it into its own SRGB and obtain label Ld_b (103). It also
uses the SID and index it into D's SRGB to obtain label Ld_d (103). Now it
programs its label forwarding state with
(Ld_b->outgoing if_bd swap to label Ld_d).
    </t>
    <t>
Similarly, D programs a label forwarding state (Ld_d->pop and lookup next
header).
    </t>
    <t>
It is clear that the following needs to happen:
       <list style="symbols">
    <t>
Each node's SRGB needs to be signalled to all other adjacent nodes
    </t>
    <t>
Each node's Node SID needs to be signalled to all other nodes
    </t>
    <t>
Each node's loopback address needs to be signalled to all other nodes
    </t>
       </list>
    </t>
    <t>
With ISIS/OSPF, each node's SRGB is actually flooded everywhere for simplicity.
With RIFT, North TIEs are flooded all the way north but South TIEs are only
flooded one hop south (and then reflected one hop north).  While the Node TIEs
could be used to flood SRGBs, each node would need to learn its own SRGB first.
With RIFT ZTP, the TOF nodes learn the SRGB and Node SID provisioning for every
node (from SR controllers) and flood them southbound via K-V distribution -
there is no need to flood SRGB via Node TIEs any more. K-V distribution is
selective and allows south nodes only to learn adjacent nodes SRGBs. 
    </t>
    <t>
For the purpose of SR-TE, a label stack is used - each entry in the stack
represents a node on the TE path towards the destination.  Consider the
following 4-level topology:
    <figure>
	<artwork>

                      TOF1                      TOF2

             Spine1_11    Spine1_21    Spine1_21    Spine1_22
					      						    
             Spine2_11    Spine2_21    Spine2_21    Spine2_22
					      
               Leaf11       Leaf12       Leaf21       Leaf22

	</artwork>
    </figure>
    </t>
    <t>
Say the TE controller instructs Leaf11 to send a packet to Spine2_11 with
label stack (Label_TOF2, Label_Spine2_21, Label_Leaf21).  Spine2_11 needs to
recognize that Label_TOF2 maps to node TOF2 and it should not simply follow
the default route (because the default route could lead to an unintended
path via TOF1). In other words, each node needs to have a specific route to
every node in the north.  That means for RIFT that the southbound distance
vector routing needs to additionally advertise routes for loopback address
of the nodes in the north.  Each node originates a route for its own loopback
address and advertises it southbound, with a special marking that allows a
south node to re-advertise it further south.
    </t>
    </section>
    <section title="Specifications">
    <t>
This document defines the following Key-Value for SR purpose, in the form of (key type, key value, value). The key type is SR, The key value is the loopback address, and the value is a (SRGB label base, SRGB label range, Node SID) tuple.  This also assumes that each node learns its own loopback address somehow, probably through another KV (given the ZTP consideration).  
    </t>
    </section>
    <section title="Security Considerations">
      <t>To be provided.
      </t>
    </section>
    <section anchor="Acknowledgements" title="Acknowledgements">
      <t>The authors thank Bruno Rijsman and Antoni Przygenda
         for their review and suggestions.
      </t>
    </section>
  </middle>

  <back>
    <references title="Normative References">
	  &RFC2119;
      <?rfc include='reference.I-D.ietf-rift-rift'?>
    </references>

    <references title="Informative References">
      &RFC8402;
      <?rfc include='reference.I-D.ietf-ospf-segment-routing-extensions'?>
      <?rfc include='reference.I-D.ietf-isis-segment-routing-extensions'?>
    </references>
  </back>
</rfc>

